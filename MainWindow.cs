﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonoApp1
{
    public partial class MainWindow : Form
    {
        private const string ACTION_IN_PROGRESS = " ...";
        private const string ACTION_CANCELLED = " cancelled";
        private const string ACTION_DONE = " done";

        public MainWindow() 
        {
            try
            {
                InitializeComponent();

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

        }

        #region Events
        #region Form Events

        private void View_Load(Object sender, EventArgs e)
        {
            try
            {
                Console.WriteLine("View_Load");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        private void View_FormClosing(Object sender, FormClosingEventArgs e)
        {
            bool resultCancel = false;

            try
            {
                Console.WriteLine("View_FormClosing");

                FileQuitAction(ref resultCancel);
                // Console.WriteLine("cancel:" + resultCancel);

                ((System.ComponentModel.CancelEventArgs)e).Cancel = resultCancel;

                if (!resultCancel)
                {
                    //clean up data model here

                    //Application.Instance.Quit();//this will only re-trigger the Closing event; just allow window to close
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

        }
        #endregion Form Events

        #region Menu Events
        private async void MenuFileNew_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "New" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuFileNew.Image); 
            await FileNewAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuFileOpen_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Open" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuFileOpen.Image); 
            FileOpenAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuFileSave_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Save" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuFileSave.Image); 
            FileSaveAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuFileSaveAs_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Save As" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("SaveAs"); 
            FileSaveAsAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuFilePrint_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Print" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuFilePrint.Image); 
            FilePrintAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuFilePrintPreview_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Print Preview" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("PrintPreview"); 
            await FilePrintPreviewAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuFileQuit_Click(object sender, EventArgs e)
		{
            this.Close();
        }
        private async void MenuEditUndo_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Undo" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditUndo.Image); 
            await EditUndoAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditRedo_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Redo" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditRedo.Image); 
            await EditRedoAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditSelectAll_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Select All" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("SelectAll"); 
            await EditSelectAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCut_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Cut" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditCut.Image); 
            await EditCutAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditCopy_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Copy" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditCopy.Image); 
            await EditCopyAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPaste_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Paste" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditPaste.Image); 
            await EditPasteAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditPasteSpecial_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Paste Special" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("PasteSpecial"); 
            await EditPasteSpecialAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditDelete_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Delete" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditDelete.Image); 
            await EditDeleteAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditFind_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Find" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditFind.Image); 
            await EditFindAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditReplace_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Replace" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditReplace.Image); 
            await EditReplaceAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditRefresh_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Refresh" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditRefresh.Image); 
            await EditRefreshAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuEditPreferences_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Preferences" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditPreferences.Image); 
            EditPreferencesAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuEditProperties_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Properties" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuEditProperties.Image); 
            await EditPropertiesAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowNewWindow_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "New Window" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("NewWindow"); 
            await WindowNewWindowAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowTile_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Tile" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Tile"); 
            await WindowTileAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowCascade_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Cascade" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Cascade"); 
            await WindowCascadeAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowArrangeAll_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Arrange All" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("ArrangeAll"); 
            await WindowArrangeAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowHide_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Hide" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Hide"); 
            await WindowHideAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuWindowShow_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Show" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Show"); 
            await WindowShowAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpContents_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Contents" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuHelpContents.Image); 
            await HelpContentsAction();
            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpIndex_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Index" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Index"); 
            await HelpIndexAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpOnlineHelp_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Online Help" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("OnlineHelp"); 
            await HelpOnlineHelpAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpLicenceInformation_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Licence Information" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("LicenceInformation"); 
            await HelpLicenceInformationAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private async void MenuHelpCheckForUpdates_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Check For Updates" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("CheckForUpdates"); 
            await HelpCheckForUpdatesAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void MenuHelpAbout_Click(object sender, EventArgs e)
        {
            HelpAboutAction();
        }
        #endregion Menu Events

        #region Toolbar Events
        #endregion Toolbar Events

        #region Control Events
        private async void cmdRun_Click(Object sender, EventArgs e)
        {
            StatusBarStatusMessage.Text = "Do Something" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon(""); 
            await DoSomething();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void Any_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(this, "I was clicked!");
            StatusBarStatusMessage.Text = "I was clicked! " + DateTime.Now.ToLongTimeString();
        }
        private void cmdColor_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Color" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Color"); 
            ColorAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void cmdFont_Click(object sender, EventArgs e)
		{
            StatusBarStatusMessage.Text = "Font" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Font"); 
            FontAction();
            // StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        #endregion Control Events
        #endregion Events

        #region Methods
        #region Actions
        private async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //StatusBarProgressBar.Pulse();
                //DoEvents;
                await Task.Delay(1000); 
            }
        }

        private async Task FileNewAction()
        {
            await DoSomething();
        }
        private void FileOpenAction()
        {
            OpenFileDialog openFileDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = false;

                response = openFileDialog.ShowDialog(this); 
                
                if (response == DialogResult.OK)
                {
                    StatusBarStatusMessage.Text += openFileDialog.FileName + ACTION_IN_PROGRESS;
                }
                else if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
            
                openFileDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void FileSaveAction(bool isSaveAs=false)
        {
            SaveFileDialog saveFileDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = (isSaveAs ? "Save As..." : "Save...");

                response = saveFileDialog.ShowDialog(this); 
                
                if (response == DialogResult.OK)
                {
                    StatusBarStatusMessage.Text += saveFileDialog.FileName + ACTION_IN_PROGRESS;
                }
                else if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
            
                saveFileDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private void FileSaveAsAction()
        {
            FileSaveAction(true);
        }
        private void FilePrintAction()
        {
            string errorMessage = null;
            PrinterSettings printer = null;
            if (GetPrinter(ref printer, ref errorMessage))
            {
                StatusBarStatusMessage.Text += printer.PrinterName;
                Console.WriteLine("PrintSettings.PrinterName:"+printer.PrinterName);
                // Note: there is no actual output from printer.PrinterName
            }
        }
        private async Task FilePrintPreviewAction()
        {
            await DoSomething();
        }
        private void FileQuitAction
        (
            ref bool resultCancel
        )
        {
            DialogResult response = DialogResult.None;

            StatusBarStatusMessage.Text = "Quit" + ACTION_IN_PROGRESS;
            StartProgressBar();
            // StartActionIcon("Quit"); 

            response = MessageBox.Show
            (
                owner: this,
                text: "Quit?",
                caption: this.Text,
                buttons: MessageBoxButtons.YesNo,
                icon: MessageBoxIcon.Question,
                defaultButton: MessageBoxDefaultButton.Button1
            );

            if (response == DialogResult.No)
            {
                resultCancel = true;

                // StopActionIcon(); 
                StopProgressBar();
                StatusBarStatusMessage.Text += ACTION_CANCELLED;//ACTION_DONE;
            }

        }
        private async Task EditUndoAction()
        {
            await DoSomething();
        }
        private async Task EditRedoAction()
        {
            await DoSomething();
        }
        private async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private async Task EditCutAction()
        {
            await DoSomething();
        }
        private async Task EditCopyAction()
        {
            await DoSomething();
        }
        private async Task EditPasteAction()
        {
            await DoSomething();
        }
        private async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private async Task EditFindAction()
        {
            await DoSomething();
        }
        private async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private async Task EditRefreshAction()
        {
            await DoSomething();
        }
        private void EditPreferencesAction()
        {
            //do a folder-path dialog demo for preferences
            FolderBrowserDialog folderBrowserDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                folderBrowserDialog = new FolderBrowserDialog();
                // folderBrowserDialog.Title = this.Title;

                response = folderBrowserDialog.ShowDialog(this); 
                
                if (response == DialogResult.OK)
                {
                    StatusBarStatusMessage.Text += folderBrowserDialog.SelectedPath + ACTION_IN_PROGRESS;
                }
                else //if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
            
                folderBrowserDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task EditPropertiesAction()
        {
            await DoSomething();
        }
        private async Task WindowNewWindowAction()
        {
            await DoSomething();
        }
        private async Task WindowTileAction()
        {
            await DoSomething();
        }
        private async Task WindowCascadeAction()
        {
            await DoSomething();
        }
        private async Task WindowArrangeAllAction()
        {
            await DoSomething();
        }
        private async Task WindowHideAction()
        {
            await DoSomething();
        }
        private async Task WindowShowAction()
        {
            await DoSomething();
        }
        private async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private async Task HelpIndexAction()
        {
            await DoSomething();
        }
        private async Task HelpOnlineHelpAction()
        {
            await DoSomething();
        }
        private async Task HelpLicenceInformationAction()
        {
            await DoSomething();
        }
        private async Task HelpCheckForUpdatesAction()
        {
            await DoSomething();
        }
        private void HelpAboutAction()
        {
            // DialogResult response = DialogResult.None;
            string message = null;

            StatusBarStatusMessage.Text = "About" + ACTION_IN_PROGRESS;
            StartProgressBar();
            StartActionIcon(menuHelpAbout.Image); 

            // aboutDialog.Logo = GetIconFromBitmap(GetBitmapFromFile("../EtoFormsApp2/Resources/App.png"), 32, 32);
            message += "ProgramName = MonoApp1\n";
            message += "Version = 0.1\n";
            message += "Desktop GUI app prototype, on Linux(/Win/Mac), in C# / Mono / WinForms, using VSCode\n";
            message += "WebsiteLabel = GitLab\n";
            message += "Website = http://www.gitlab.com\n";
            // // aboutDialog.Platform;r/o
            message += "Copyright = Copyright (C) 2022 Stephen J Sepan\n";
            message += "Designers = Stephen J Sepan, Designer2, Designer3\n";
            message += "Developers = Stephen J Sepan, Developer2, Developer3\n";
            message += "Documenters = Stephen J Sepan, Documenter2, Documenter3\n";
            message += "License = License text here\n";

            _/* response */ = MessageBox.Show
            (
                owner: this,
                text: message,
                caption: "About " + this.Text,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information,
                defaultButton: MessageBoxDefaultButton.Button1
            );

            StopActionIcon(); 
            StopProgressBar();
            StatusBarStatusMessage.Text += ACTION_DONE;
        }
        private void ColorAction()
        {
            string errorMessage = null;
            System.Drawing.Color color = System.Drawing.Color.Black;
            if (GetColor(ref color, ref errorMessage))
            {
                StatusBarStatusMessage.Text += color.ToString() + ACTION_IN_PROGRESS;
            }
        }
        private void FontAction()
        {
            string errorMessage = null;
            System.Drawing.Font font = new System.Drawing.Font("Ubuntu Regular", 10);
            System.Drawing.Color color = new System.Drawing.Color();
            if (GetFont(ref font, ref color, ref errorMessage))
            {
                StatusBarStatusMessage.Text += font.ToString() + "," + color.ToString() + ACTION_IN_PROGRESS;
            }
        }
        #endregion Actions

        #region Utility
        private void StartProgressBar()
        {
            StatusBarProgressBar.Value = 33;
            StatusBarProgressBar.Style = ProgressBarStyle.Marquee;//true;
            StatusBarProgressBar.Visible = true;
            //DoEvents;
        }

        private void StopProgressBar()
        {
            //DoEvents;
            StatusBarProgressBar.Visible = false;
        }

        private void StartActionIcon(Image resourceItem/*string resourceItemId*/)
        {
            StatusBarActionIcon.Image = (resourceItem == null ? null : resourceItem);//global::MonoApp1.Properties.Resources.New; //"New", etc.
            StatusBarActionIcon.Visible = true;
        }

        private void StopActionIcon()
        {
            StatusBarActionIcon.Visible = false;
            StatusBarActionIcon.Image = global::MonoApp1.Properties.Resources.New; 
        }

        private bool GetPrinter
        (
            ref PrinterSettings printer,
            ref string errorMessage
        )
        {
            bool returnValue = false;
            PrintDialog printDialog = new PrintDialog();
            DialogResult response = DialogResult.None;
            
            try
            {
                response = printDialog.ShowDialog(this);
                    
                if (response == DialogResult.OK)
                {
                    printer = printDialog.PrinterSettings;
                }
                else //if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
                
                printDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }

        /// <summary>
        /// Get a color.
        /// </summary>
        /// <param name="color"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool GetColor
        (
            ref System.Drawing.Color color,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            ColorDialog colorDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                colorDialog = new ColorDialog();

                response = colorDialog.ShowDialog(this);
                
                if (response == DialogResult.OK)
                {
                    color = colorDialog.Color;
                    returnValue = true;
                }
                else //if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
            
                colorDialog.Dispose();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }
    
        /// <summary>
        /// Get a font.
        /// </summary>
        /// <param name="font"></param>
        /// <param name="color"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool GetFont
        (
            ref System.Drawing.Font font,
            ref System.Drawing.Color color,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            FontDialog fontDialog = null;
            DialogResult response = DialogResult.None;

            try
            {
                // fontResponse = null;
                fontDialog = new FontDialog();

                response = fontDialog.ShowDialog(this);
                
                if (response == DialogResult.OK)
                {
                    font = fontDialog.Font;
                    color = fontDialog.Color;   
                    returnValue = true;
                }
                else //if (response == DialogResult.Cancel)
                {
                    StatusBarStatusMessage.Text += ACTION_CANCELLED + ACTION_IN_PROGRESS;
                }
            
                fontDialog.Dispose();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }
        #endregion Utility
        #endregion Methods


    }
}
