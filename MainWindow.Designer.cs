namespace MonoApp1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFilePrint = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditSeparator0 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditCut = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditFind = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuEditPreferences = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowNewWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowTile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowArrangeAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuWindowHide = new System.Windows.Forms.ToolStripMenuItem();
            this.menuWindowShow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpContents = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpIndex = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpOnlineHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHelpLicenceInformation = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpCheckForUpdates = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelpSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolbar = new System.Windows.Forms.ToolStrip();
            this.buttonFileNew = new System.Windows.Forms.ToolStripButton();
            this.buttonFileOpen = new System.Windows.Forms.ToolStripButton();
            this.buttonFileSave = new System.Windows.Forms.ToolStripButton();
            this.buttonFilePrint = new System.Windows.Forms.ToolStripButton();
            this.buttonSeparator0 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonEditUndo = new System.Windows.Forms.ToolStripButton();
            this.buttonEditRedo = new System.Windows.Forms.ToolStripButton();
            this.buttonEditCut = new System.Windows.Forms.ToolStripButton();
            this.buttonEditCopy = new System.Windows.Forms.ToolStripButton();
            this.buttonEditPaste = new System.Windows.Forms.ToolStripButton();
            this.buttonEditDelete = new System.Windows.Forms.ToolStripButton();
            this.buttonEditFind = new System.Windows.Forms.ToolStripButton();
            this.buttonEditReplace = new System.Windows.Forms.ToolStripButton();
            this.buttonEditRefresh = new System.Windows.Forms.ToolStripButton();
            this.buttonEditPreferences = new System.Windows.Forms.ToolStripButton();
            this.buttonEditProperties = new System.Windows.Forms.ToolStripButton();
            this.buttonSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonHelpContents = new System.Windows.Forms.ToolStripButton();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.StatusBarStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarErrorMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarCustomMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.StatusBarActionIcon = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarDirtyMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusBarNetworkIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblSomeInt = new System.Windows.Forms.Label();
            this.lblSomeString = new System.Windows.Forms.Label();
            this.lblSomeBoolean = new System.Windows.Forms.Label();
            this.txtSomeInt = new System.Windows.Forms.TextBox();
            this.txtSomeString = new System.Windows.Forms.TextBox();
            this.chkSomeBoolean = new System.Windows.Forms.CheckBox();
            this.cmdRun = new System.Windows.Forms.Button();
            this.lblSomeOtherInt = new System.Windows.Forms.Label();
            this.lblSomeOtherString = new System.Windows.Forms.Label();
            this.lblSomeOtherBoolean = new System.Windows.Forms.Label();
            this.txtSomeOtherInt = new System.Windows.Forms.TextBox();
            this.txtSomeOtherString = new System.Windows.Forms.TextBox();
            this.chkSomeOtherBoolean = new System.Windows.Forms.CheckBox();
            this.lblStillAnotherInt = new System.Windows.Forms.Label();
            this.lblStillAnotherString = new System.Windows.Forms.Label();
            this.lblStillAnotherBoolean = new System.Windows.Forms.Label();
            this.txtStillAnotherInt = new System.Windows.Forms.TextBox();
            this.txtStillAnotherString = new System.Windows.Forms.TextBox();
            this.chkStillAnotherBoolean = new System.Windows.Forms.CheckBox();
            this.cmdFont = new System.Windows.Forms.Button();
            this.cmdColor = new System.Windows.Forms.Button();
            this.toolbar.SuspendLayout();
            this.menu.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // // 
            // // menuWindowSeparator2
            // // 
            // this.menuWindowSeparator2.Name = "menuWindowSeparator2";
            // this.menuWindowSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuEdit,
            this.menuWindow,
            this.menuHelp});
            this.menu.Dock = System.Windows.Forms.DockStyle.Top;
            // this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            // this.menu.Size = new System.Drawing.Size(624, 24);
            this.menu.TabIndex = 117;
            this.menu.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.menuFileOpen,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.menuFileSeparator1,
            this.menuFilePrint,
            this.menuFileSeparator2,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "&File";
            // 
            // menuFileNew
            // 
            this.menuFileNew.Image = global::MonoApp1.Properties.Resources.New;
            this.menuFileNew.Name = "menuFileNew";
            this.menuFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuFileNew.Size = new System.Drawing.Size(149, 22);
            this.menuFileNew.Text = "&New";
            this.menuFileNew.Click += new System.EventHandler(this.MenuFileNew_Click);
            // 
            // menuFileOpen
            // 
            this.menuFileOpen.Image = global::MonoApp1.Properties.Resources.Open;
            this.menuFileOpen.Name = "menuFileOpen";
            this.menuFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuFileOpen.Size = new System.Drawing.Size(149, 22);
            this.menuFileOpen.Text = "&Open";
            this.menuFileOpen.Click += new System.EventHandler(this.MenuFileOpen_Click);
            // 
            // menuFileSave
            // 
            this.menuFileSave.Image = global::MonoApp1.Properties.Resources.Save;
            this.menuFileSave.Name = "menuFileSave";
            this.menuFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuFileSave.Size = new System.Drawing.Size(149, 22);
            this.menuFileSave.Text = "&Save";
            this.menuFileSave.Click += new System.EventHandler(this.MenuFileSave_Click);
            // 
            // menuFileSaveAs
            // 
            this.menuFileSaveAs.Name = "menuFileSaveAs";
            this.menuFileSaveAs.Size = new System.Drawing.Size(149, 22);
            this.menuFileSaveAs.Text = "Save &As...";
            this.menuFileSaveAs.Click += new System.EventHandler(this.MenuFileSaveAs_Click);
            // 
            // menuFileSeparator1
            // 
            this.menuFileSeparator1.Name = "menuFileSeparator1";
            this.menuFileSeparator1.Size = new System.Drawing.Size(146, 6);
            // 
            // menuFilePrint
            // 
            this.menuFilePrint.Image = global::MonoApp1.Properties.Resources.Print;
            this.menuFilePrint.Name = "menuFilePrint";
            this.menuFilePrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.menuFilePrint.Size = new System.Drawing.Size(149, 22);
            this.menuFilePrint.Text = "&Print...";
            this.menuFilePrint.Click += new System.EventHandler(this.MenuFilePrint_Click);
            // 
            // menuFileSeparator2
            // 
            this.menuFileSeparator2.Name = "menuFileSeparator2";
            this.menuFileSeparator2.Size = new System.Drawing.Size(146, 6);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.menuFileExit.Size = new System.Drawing.Size(149, 22);
            this.menuFileExit.Text = "E&xit";
            this.menuFileExit.Click += new System.EventHandler(this.MenuFileQuit_Click);
            // 
            // menuEdit
            // 
            this.menuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEditUndo,
            this.menuEditRedo,
            this.menuEditSeparator0,
            this.menuEditSelectAll,
            this.menuEditCut,
            this.menuEditCopy,
            this.menuEditPaste,
            this.menuEditDelete,
            this.menuEditSeparator1,
            this.menuEditFind,
            this.menuEditReplace,
            this.menuEditSeparator2,
            this.menuEditRefresh,
            this.menuEditSeparator3,
            this.menuEditPreferences,
            this.menuEditProperties});
            this.menuEdit.Name = "menuEdit";
            this.menuEdit.Size = new System.Drawing.Size(39, 20);
            this.menuEdit.Text = "&Edit";
            // 
            // menuEditUndo
            // 
            this.menuEditUndo.Image = global::MonoApp1.Properties.Resources.Undo;
            this.menuEditUndo.Name = "menuEditUndo";
            this.menuEditUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.menuEditUndo.Size = new System.Drawing.Size(167, 22);
            this.menuEditUndo.Text = "&Undo";
            this.menuEditUndo.Click += new System.EventHandler(this.MenuEditUndo_Click);
            // 
            // menuEditRedo
            // 
            this.menuEditRedo.Image = global::MonoApp1.Properties.Resources.Redo;
            this.menuEditRedo.Name = "menuEditRedo";
            this.menuEditRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.menuEditRedo.Size = new System.Drawing.Size(167, 22);
            this.menuEditRedo.Text = "&Redo";
            this.menuEditRedo.Click += new System.EventHandler(this.MenuEditRedo_Click);
            // 
            // menuEditSeparator0
            // 
            this.menuEditSeparator0.Name = "menuEditSeparator0";
            this.menuEditSeparator0.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditSelectAll
            // 
            this.menuEditSelectAll.Name = "menuEditSelectAll";
            this.menuEditSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.menuEditSelectAll.Size = new System.Drawing.Size(167, 22);
            this.menuEditSelectAll.Text = "Select &All";
            this.menuEditSelectAll.Click += new System.EventHandler(this.MenuEditSelectAll_Click);
            // 
            // menuEditCut
            // 
            this.menuEditCut.Image = global::MonoApp1.Properties.Resources.Cut;
            this.menuEditCut.Name = "menuEditCut";
            this.menuEditCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuEditCut.Size = new System.Drawing.Size(167, 22);
            this.menuEditCut.Text = "Cu&t";
            this.menuEditCut.Click += new System.EventHandler(this.MenuEditCut_Click);
            // 
            // menuEditCopy
            // 
            this.menuEditCopy.Image = global::MonoApp1.Properties.Resources.Copy;
            this.menuEditCopy.ImageTransparentColor = System.Drawing.Color.Black;
            this.menuEditCopy.Name = "menuEditCopy";
            this.menuEditCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuEditCopy.Size = new System.Drawing.Size(167, 22);
            this.menuEditCopy.Text = "&Copy";
            this.menuEditCopy.Click += new System.EventHandler(this.MenuEditCopy_Click);
            // 
            // menuEditPaste
            // 
            this.menuEditPaste.Image = global::MonoApp1.Properties.Resources.Paste;
            this.menuEditPaste.Name = "menuEditPaste";
            this.menuEditPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuEditPaste.Size = new System.Drawing.Size(167, 22);
            this.menuEditPaste.Text = "&Paste";
            this.menuEditPaste.Click += new System.EventHandler(this.MenuEditPaste_Click);
            // 
            // menuEditDelete
            // 
            this.menuEditDelete.Image = global::MonoApp1.Properties.Resources.Delete;
            this.menuEditDelete.Name = "menuEditDelete";
            this.menuEditDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.menuEditDelete.Size = new System.Drawing.Size(167, 22);
            this.menuEditDelete.Text = "&Delete";
            this.menuEditDelete.Click += new System.EventHandler(this.MenuEditDelete_Click);
            // 
            // menuEditSeparator1
            // 
            this.menuEditSeparator1.Name = "menuEditSeparator1";
            this.menuEditSeparator1.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditFind
            // 
            this.menuEditFind.Image = global::MonoApp1.Properties.Resources.Find;
            this.menuEditFind.Name = "menuEditFind";
            this.menuEditFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.menuEditFind.Size = new System.Drawing.Size(167, 22);
            this.menuEditFind.Text = "&Find...";
            this.menuEditFind.Click += new System.EventHandler(this.MenuEditFind_Click);
            // 
            // menuEditReplace
            // 
            this.menuEditReplace.Image = global::MonoApp1.Properties.Resources.Replace;
            this.menuEditReplace.Name = "menuEditReplace";
            this.menuEditReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.menuEditReplace.Size = new System.Drawing.Size(167, 22);
            this.menuEditReplace.Text = "Rep&lace...";
            this.menuEditReplace.Click += new System.EventHandler(this.MenuEditReplace_Click);
            // 
            // menuEditSeparator2
            // 
            this.menuEditSeparator2.Name = "menuEditSeparator2";
            this.menuEditSeparator2.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditRefresh
            // 
            this.menuEditRefresh.Image = global::MonoApp1.Properties.Resources.Reload;
            this.menuEditRefresh.Name = "menuEditRefresh";
            this.menuEditRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.menuEditRefresh.Size = new System.Drawing.Size(167, 22);
            this.menuEditRefresh.Text = "R&efresh";
            this.menuEditRefresh.Click += new System.EventHandler(this.MenuEditRefresh_Click);
            // 
            // menuEditSeparator3
            // 
            this.menuEditSeparator3.Name = "menuEditSeparator3";
            this.menuEditSeparator3.Size = new System.Drawing.Size(164, 6);
            // 
            // menuEditPreferences
            // 
            this.menuEditPreferences.Image = global::MonoApp1.Properties.Resources.Preferences;
            this.menuEditPreferences.Name = "menuEditPreferences";
            this.menuEditPreferences.Size = new System.Drawing.Size(167, 22);
            this.menuEditPreferences.Text = "Prefere&nces...";
            this.menuEditPreferences.Click += new System.EventHandler(this.MenuEditPreferences_Click);
            // 
            // menuEditProperties
            // 
            this.menuEditProperties.Image = global::MonoApp1.Properties.Resources.Properties;
            this.menuEditProperties.ImageTransparentColor = System.Drawing.Color.Black;
            this.menuEditProperties.Name = "menuEditProperties";
            this.menuEditProperties.Size = new System.Drawing.Size(167, 22);
            this.menuEditProperties.Text = "Pr&operties...";
            this.menuEditProperties.Click += new System.EventHandler(this.MenuEditProperties_Click);
            //
            // menuWindow
            //
            this.menuWindow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuWindowNewWindow,
            this.menuWindowTile,
            this.menuWindowCascade,
            this.menuWindowArrangeAll,
            this.menuWindowSeparator1,
            this.menuWindowHide,
            this.menuWindowShow});
            this.menuWindow.Name = "menuWindow";
            this.menuWindow.Size = new System.Drawing.Size(39, 20);
            this.menuWindow.Text = "&Window";
            //
            // menuWindowNewWindow
            //
            this.menuWindowNewWindow.Name = "menuWindowNewWindow";
            // this.menuWindowNewWindow.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowNewWindow.Size = new System.Drawing.Size(167, 22);
            this.menuWindowNewWindow.Text = "&New Window";
            this.menuWindowNewWindow.Click += new System.EventHandler(this.MenuWindowNewWindow_Click);
            //
            // menuWindowTile
            //
            this.menuWindowTile.Name = "menuWindowTile";
            // this.menuWindowTile.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowTile.Size = new System.Drawing.Size(167, 22);
            this.menuWindowTile.Text = "&Tile";
            this.menuWindowTile.Click += new System.EventHandler(this.MenuWindowTile_Click);
            //
            // menuWindowCascade
            //
            this.menuWindowCascade.Name = "menuWindowCascade";
            // this.menuWindowCascade.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowCascade.Size = new System.Drawing.Size(167, 22);
            this.menuWindowCascade.Text = "&Cascade";
            this.menuWindowCascade.Click += new System.EventHandler(this.MenuWindowCascade_Click);
            //
            // menuWindowArrangeAll
            //
            this.menuWindowArrangeAll.Name = "menuWindowArrangeAll";
            // this.menuWindowArrangeAll.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowArrangeAll.Size = new System.Drawing.Size(167, 22);
            this.menuWindowArrangeAll.Text = "&Arrange All";
            this.menuWindowArrangeAll.Click += new System.EventHandler(this.MenuWindowArrangeAll_Click);
            //
            // menuWindowSeparator1
            //
            this.menuWindowSeparator1.Name = "menuWindowSeparator1";
            this.menuWindowSeparator1.Size = new System.Drawing.Size(164, 6);
            //
            // menuWindowHide
            //
            this.menuWindowHide.Name = "menuWindowHide";
            // this.menuWindowHide.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowHide.Size = new System.Drawing.Size(167, 22);
            this.menuWindowHide.Text = "&Hide";
            this.menuWindowHide.Click += new System.EventHandler(this.MenuWindowHide_Click);
            //
            // menuWindowShow
            //
            this.menuWindowShow.Name = "menuWindowShow";
            // this.menuWindowShow.ShortcutKeys = ((System.Windows.Forms.Keys)(System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q));
            this.menuWindowShow.Size = new System.Drawing.Size(167, 22);
            this.menuWindowShow.Text = "&Show";
            this.menuWindowShow.Click += new System.EventHandler(this.MenuWindowShow_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHelpContents,
            this.menuHelpIndex,
            this.menuHelpOnlineHelp,
            this.menuHelpSeparator1,
            this.menuHelpLicenceInformation,
            this.menuHelpCheckForUpdates,
            this.menuHelpSeparator2,
            this.menuHelpAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(44, 20);
            this.menuHelp.Text = "&Help";
            this.menuHelp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuHelpContents
            // 
            this.menuHelpContents.Image = global::MonoApp1.Properties.Resources.Contents;
            this.menuHelpContents.Name = "menuHelpContents";
            this.menuHelpContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.menuHelpContents.Size = new System.Drawing.Size(181, 22);
            this.menuHelpContents.Text = "&Contents";
            this.menuHelpContents.Click += new System.EventHandler(this.MenuHelpContents_Click);
            // 
            // menuHelpIndex
            // 
            this.menuHelpIndex.Name = "menuHelpIndex";
            this.menuHelpIndex.Size = new System.Drawing.Size(181, 22);
            this.menuHelpIndex.Text = "&Index";
            this.menuHelpIndex.Click += new System.EventHandler(this.MenuHelpIndex_Click);
            // 
            // menuHelpOnlineHelp
            // 
            this.menuHelpOnlineHelp.Name = "menuHelpOnlineHelp";
            this.menuHelpOnlineHelp.Size = new System.Drawing.Size(181, 22);
            this.menuHelpOnlineHelp.Text = "&Online Help";
            this.menuHelpOnlineHelp.Click += new System.EventHandler(this.MenuHelpOnlineHelp_Click);
            // 
            // menuHelpSeparator1
            // 
            this.menuHelpSeparator1.Name = "menuHelpSeparator1";
            this.menuHelpSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // menuHelpLicenceInformation
            // 
            this.menuHelpLicenceInformation.Name = "menuHelpLicenceInformation";
            this.menuHelpLicenceInformation.Size = new System.Drawing.Size(181, 22);
            this.menuHelpLicenceInformation.Text = "&Licence Information";
            this.menuHelpLicenceInformation.Click += new System.EventHandler(this.MenuHelpLicenceInformation_Click);
            // 
            // menuHelpCheckForUpdates
            // 
            this.menuHelpCheckForUpdates.Name = "menuHelpCheckForUpdates";
            this.menuHelpCheckForUpdates.Size = new System.Drawing.Size(181, 22);
            this.menuHelpCheckForUpdates.Text = "Check for &Updates";
            this.menuHelpCheckForUpdates.Click += new System.EventHandler(this.MenuHelpCheckForUpdates_Click);
            // 
            // menuHelpSeparator2
            // 
            this.menuHelpSeparator2.Name = "menuHelpSeparator2";
            this.menuHelpSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // menuHelpAbout
            // 
            this.menuHelpAbout.Image = global::MonoApp1.Properties.Resources.About;
            this.menuHelpAbout.Name = "menuHelpAbout";
            this.menuHelpAbout.Size = new System.Drawing.Size(181, 22);
            this.menuHelpAbout.Text = "&About MonoApp1 ...";
            this.menuHelpAbout.Click += new System.EventHandler(this.MenuHelpAbout_Click);
            // 
            // toolbar
            // 
            this.toolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonFileNew,
            this.buttonFileOpen,
            this.buttonFileSave,
            this.buttonFilePrint,
            this.buttonSeparator0,
            this.buttonEditUndo,
            this.buttonEditRedo,
            this.buttonEditCut,
            this.buttonEditCopy,
            this.buttonEditPaste,
            this.buttonEditDelete,
            this.buttonEditFind,
            this.buttonEditReplace,
            this.buttonEditRefresh,
            this.buttonEditPreferences,
            this.buttonEditProperties,
            this.buttonSeparator1,
            this.buttonHelpContents});
            this.toolbar.Dock = System.Windows.Forms.DockStyle.Top;
            // this.toolbar.Location = new System.Drawing.Point(0, 24);
            this.toolbar.Name = "toolbar";
            // this.toolbar.Size = new System.Drawing.Size(624, 25);
            this.toolbar.TabIndex = 118;
            this.toolbar.Text = "toolStrip1";
            // 
            // buttonFileNew
            // 
            this.buttonFileNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFileNew.Image = global::MonoApp1.Properties.Resources.New;
            this.buttonFileNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFileNew.Name = "buttonFileNew";
            this.buttonFileNew.Size = new System.Drawing.Size(23, 22);
            this.buttonFileNew.Text = "&New";
            this.buttonFileNew.Click += new System.EventHandler(this.MenuFileNew_Click);
            // 
            // buttonFileOpen
            // 
            this.buttonFileOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFileOpen.Image = global::MonoApp1.Properties.Resources.Open;
            this.buttonFileOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFileOpen.Name = "buttonFileOpen";
            this.buttonFileOpen.Size = new System.Drawing.Size(23, 22);
            this.buttonFileOpen.Text = "&Open";
            this.buttonFileOpen.Click += new System.EventHandler(this.MenuFileOpen_Click);
            // 
            // buttonFileSave
            // 
            this.buttonFileSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFileSave.Image = global::MonoApp1.Properties.Resources.Save;
            this.buttonFileSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFileSave.Name = "buttonFileSave";
            this.buttonFileSave.Size = new System.Drawing.Size(23, 22);
            this.buttonFileSave.Text = "&Save";
            this.buttonFileSave.Click += new System.EventHandler(this.MenuFileSave_Click);
            // 
            // buttonFilePrint
            // 
            this.buttonFilePrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonFilePrint.Image = global::MonoApp1.Properties.Resources.Print;
            this.buttonFilePrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonFilePrint.Name = "buttonFilePrint";
            this.buttonFilePrint.Size = new System.Drawing.Size(23, 22);
            this.buttonFilePrint.Text = "Print";
            this.buttonFilePrint.Click += new System.EventHandler(this.MenuFilePrint_Click);
            // 
            // buttonSeparator0
            // 
            this.buttonSeparator0.Name = "buttonSeparator0";
            this.buttonSeparator0.Size = new System.Drawing.Size(6, 25);
            // 
            // buttonEditUndo
            // 
            this.buttonEditUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditUndo.Image = global::MonoApp1.Properties.Resources.Undo;
            this.buttonEditUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditUndo.Name = "buttonEditUndo";
            this.buttonEditUndo.Size = new System.Drawing.Size(23, 22);
            this.buttonEditUndo.Text = "Undo";
            this.buttonEditUndo.Click += new System.EventHandler(this.MenuEditUndo_Click);
            // 
            // buttonEditRedo
            // 
            this.buttonEditRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditRedo.Image = global::MonoApp1.Properties.Resources.Redo;
            this.buttonEditRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditRedo.Name = "buttonEditRedo";
            this.buttonEditRedo.Size = new System.Drawing.Size(23, 22);
            this.buttonEditRedo.Text = "Redo";
            this.buttonEditRedo.Click += new System.EventHandler(this.MenuEditRedo_Click);
            // 
            // buttonEditCut
            // 
            this.buttonEditCut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditCut.Image = global::MonoApp1.Properties.Resources.Cut;
            this.buttonEditCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditCut.Name = "buttonEditCut";
            this.buttonEditCut.Size = new System.Drawing.Size(23, 22);
            this.buttonEditCut.Text = "Cut";
            this.buttonEditCut.Click += new System.EventHandler(this.MenuEditCut_Click);
            // 
            // buttonEditCopy
            // 
            this.buttonEditCopy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditCopy.Image = global::MonoApp1.Properties.Resources.Copy;
            this.buttonEditCopy.ImageTransparentColor = System.Drawing.Color.Black;
            this.buttonEditCopy.Name = "buttonEditCopy";
            this.buttonEditCopy.Size = new System.Drawing.Size(23, 22);
            this.buttonEditCopy.Text = "Copy";
            this.buttonEditCopy.Click += new System.EventHandler(this.MenuEditCopy_Click);
            // 
            // buttonEditPaste
            // 
            this.buttonEditPaste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditPaste.Image = global::MonoApp1.Properties.Resources.Paste;
            this.buttonEditPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditPaste.Name = "buttonEditPaste";
            this.buttonEditPaste.Size = new System.Drawing.Size(23, 22);
            this.buttonEditPaste.Text = "Paste";
            this.buttonEditPaste.Click += new System.EventHandler(this.MenuEditPaste_Click);
            // 
            // buttonEditDelete
            // 
            this.buttonEditDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditDelete.Image = global::MonoApp1.Properties.Resources.Delete;
            this.buttonEditDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditDelete.Name = "buttonEditDelete";
            this.buttonEditDelete.Size = new System.Drawing.Size(23, 22);
            this.buttonEditDelete.Text = "Delete";
            this.buttonEditDelete.Click += new System.EventHandler(this.MenuEditDelete_Click);
            // 
            // buttonEditFind
            // 
            this.buttonEditFind.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditFind.Image = global::MonoApp1.Properties.Resources.Find;
            this.buttonEditFind.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditFind.Name = "buttonEditFind";
            this.buttonEditFind.Size = new System.Drawing.Size(23, 22);
            this.buttonEditFind.Text = "Find";
            this.buttonEditFind.Click += new System.EventHandler(this.MenuEditFind_Click);
            // 
            // buttonEditReplace
            // 
            this.buttonEditReplace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditReplace.Image = global::MonoApp1.Properties.Resources.Replace;
            this.buttonEditReplace.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditReplace.Name = "buttonEditReplace";
            this.buttonEditReplace.Size = new System.Drawing.Size(23, 22);
            this.buttonEditReplace.Text = "Replace";
            this.buttonEditReplace.Click += new System.EventHandler(this.MenuEditReplace_Click);
            // 
            // buttonEditRefresh
            // 
            this.buttonEditRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditRefresh.Image = global::MonoApp1.Properties.Resources.Reload;
            this.buttonEditRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditRefresh.Name = "buttonEditRefresh";
            this.buttonEditRefresh.Size = new System.Drawing.Size(23, 22);
            this.buttonEditRefresh.Text = "Refresh";
            this.buttonEditRefresh.Click += new System.EventHandler(this.MenuEditRefresh_Click);
            // 
            // buttonEditPreferences
            // 
            this.buttonEditPreferences.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditPreferences.Image = global::MonoApp1.Properties.Resources.Preferences;
            this.buttonEditPreferences.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEditPreferences.Name = "buttonEditPreferences";
            this.buttonEditPreferences.Size = new System.Drawing.Size(23, 22);
            this.buttonEditPreferences.Text = "Preferences";
            this.buttonEditPreferences.Click += new System.EventHandler(this.MenuEditPreferences_Click);
            // 
            // buttonEditProperties
            // 
            this.buttonEditProperties.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonEditProperties.Image = global::MonoApp1.Properties.Resources.Properties;
            this.buttonEditProperties.ImageTransparentColor = System.Drawing.Color.Black;
            this.buttonEditProperties.Name = "buttonEditProperties";
            this.buttonEditProperties.Size = new System.Drawing.Size(23, 22);
            this.buttonEditProperties.Text = "P&roperties...";
            this.buttonEditProperties.Click += new System.EventHandler(this.MenuEditProperties_Click);
            // 
            // buttonSeparator1
            // 
            this.buttonSeparator1.Name = "buttonSeparator1";
            this.buttonSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // buttonHelpContents
            // 
            this.buttonHelpContents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonHelpContents.Image = global::MonoApp1.Properties.Resources.Contents;
            this.buttonHelpContents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonHelpContents.Name = "buttonHelpContents";
            this.buttonHelpContents.Size = new System.Drawing.Size(23, 22);
            this.buttonHelpContents.Text = "Contents";
            this.buttonHelpContents.Click += new System.EventHandler(this.MenuHelpContents_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarStatusMessage,
            this.StatusBarErrorMessage,
            this.StatusBarProgressBar,
            this.StatusBarActionIcon,
            this.StatusBarDirtyMessage,
            this.StatusBarNetworkIndicator});
            this.statusBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            // this.statusBar.Location = new System.Drawing.Point(0, 420);
            this.statusBar.Name = "statusBar";
            // this.statusBar.Size = new System.Drawing.Size(624, 22);
            this.statusBar.TabIndex = 116;
            this.statusBar.Text = "statusStrip1";
            // 
            // StatusBarStatusMessage
            // 
            this.StatusBarStatusMessage.ForeColor = System.Drawing.Color.Green;
            this.StatusBarStatusMessage.Name = "StatusBarStatusMessage";
            this.StatusBarStatusMessage.Size = new System.Drawing.Size(0, 17);
            this.StatusBarStatusMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StatusBarStatusMessage.Text = "";
            // 
            // StatusBarErrorMessage
            // 
            this.StatusBarErrorMessage.AutoToolTip = true;
            this.StatusBarErrorMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StatusBarErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.StatusBarErrorMessage.Name = "StatusBarErrorMessage";
            this.StatusBarErrorMessage.Size = new System.Drawing.Size(609, 17);
            this.StatusBarErrorMessage.Spring = true;
            this.StatusBarErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StatusBarErrorMessage.Text = "";
            // 
            // StatusBarCustomMessage
            // 
            this.StatusBarCustomMessage.Name = "StatusBarCustomMessage";
            this.StatusBarCustomMessage.Size = new System.Drawing.Size(0, 17);
            // 
            // StatusBarProgressBar
            // 
            this.StatusBarProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.StatusBarProgressBar.Name = "StatusBarProgressBar";
            this.StatusBarProgressBar.Size = new System.Drawing.Size(100, 16);
            this.StatusBarProgressBar.Value = 10;
            this.StatusBarProgressBar.Visible = false;
            // 
            // StatusBarActionIcon
            // 
            this.StatusBarActionIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarActionIcon.Image = global::MonoApp1.Properties.Resources.New;
            this.StatusBarActionIcon.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarActionIcon.Name = "StatusBarActionIcon";
            this.StatusBarActionIcon.Size = new System.Drawing.Size(16, 17);
            this.StatusBarActionIcon.Visible = false;
            // 
            // StatusBarDirtyMessage
            // 
            this.StatusBarDirtyMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarDirtyMessage.Image = global::MonoApp1.Properties.Resources.Save;
            this.StatusBarDirtyMessage.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarDirtyMessage.Name = "StatusBarDirtyMessage";
            this.StatusBarDirtyMessage.Size = new System.Drawing.Size(16, 17);
            this.StatusBarDirtyMessage.ToolTipText = "Dirty";
            this.StatusBarDirtyMessage.Visible = false;
            // 
            // StatusBarNetworkIndicator
            // 
            this.StatusBarNetworkIndicator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StatusBarNetworkIndicator.Image = global::MonoApp1.Properties.Resources.Network;
            this.StatusBarNetworkIndicator.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StatusBarNetworkIndicator.ImageTransparentColor = System.Drawing.Color.Fuchsia;
            this.StatusBarNetworkIndicator.Name = "StatusBarNetworkIndicator";
            this.StatusBarNetworkIndicator.Size = new System.Drawing.Size(16, 17);
            this.StatusBarNetworkIndicator.ToolTipText = "Network Connected";
            this.StatusBarNetworkIndicator.Visible = false;
            // 
            // lblSomeInt
            // 
            this.lblSomeInt.AutoSize = true;
            this.lblSomeInt.Location = new System.Drawing.Point(5, 65);
            this.lblSomeInt.Name = "lblSomeInt";
            this.lblSomeInt.Size = new System.Drawing.Size(31, 13);
            this.lblSomeInt.TabIndex = 119;
            this.lblSomeInt.Text = "Int:";
            // 
            // lblSomeString
            // 
            this.lblSomeString.AutoSize = true;
            this.lblSomeString.Location = new System.Drawing.Point(5, 85);
            this.lblSomeString.Name = "lblSomeString";
            this.lblSomeString.Size = new System.Drawing.Size(34, 13);
            this.lblSomeString.TabIndex = 120;
            this.lblSomeString.Text = "String:";
            //
            // lblSomeBoolean
            //
            this.lblSomeBoolean.AutoSize = true;
            this.lblSomeBoolean.Location = new System.Drawing.Point(5, 105);
            this.lblSomeBoolean.Name = "lblSomeBoolean";
            this.lblSomeBoolean.Size = new System.Drawing.Size(31, 13);
            this.lblSomeBoolean.TabIndex = 120;
            this.lblSomeBoolean.Text = "Boolean:";
            // 
            // txtSomeInt
            // 
            this.txtSomeInt.Location = new System.Drawing.Point(55, 60);
            this.txtSomeInt.Name = "txtSomeInt";
            this.txtSomeInt.Size = new System.Drawing.Size(100, 20);
            this.txtSomeInt.TabIndex = 122;
            // 
            // txtSomeString
            // 
            this.txtSomeString.Location = new System.Drawing.Point(55, 80);
            this.txtSomeString.Name = "txtSomeString";
            this.txtSomeString.Size = new System.Drawing.Size(100, 20);
            this.txtSomeString.TabIndex = 123;
            // 
            // chkSomeBoolean
            // 
            this.chkSomeBoolean.AutoSize = true;
            this.chkSomeBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSomeBoolean.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkSomeBoolean.Location = new System.Drawing.Point(55, 105);
            this.chkSomeBoolean.Name = "chkSomeBoolean";
            this.chkSomeBoolean.Size = new System.Drawing.Size(65, 17);
            this.chkSomeBoolean.TabIndex = 124;
            this.chkSomeBoolean.Text = "";
            this.chkSomeBoolean.UseVisualStyleBackColor = true;
            // 
            // cmdRun
            // 
            this.cmdRun.Location = new System.Drawing.Point(537, 125);
            this.cmdRun.Name = "cmdRun";
            this.cmdRun.Size = new System.Drawing.Size(75, 23);
            this.cmdRun.TabIndex = 125;
            this.cmdRun.Text = "Run";
            this.cmdRun.UseVisualStyleBackColor = true;
            this.cmdRun.Click += new System.EventHandler(this.cmdRun_Click);
            // 
            // lblSomeOtherInt
            // 
            this.lblSomeOtherInt.AutoSize = true;
            this.lblSomeOtherInt.Location = new System.Drawing.Point(170, 65);
            this.lblSomeOtherInt.Name = "lblSomeOtherInt";
            this.lblSomeOtherInt.Size = new System.Drawing.Size(31, 13);
            this.lblSomeOtherInt.TabIndex = 126;
            this.lblSomeOtherInt.Text = "Other Int:";
            // 
            // lblSomeOtherString
            // 
            this.lblSomeOtherString.AutoSize = true;
            this.lblSomeOtherString.Location = new System.Drawing.Point(170, 85);
            this.lblSomeOtherString.Name = "lblSomeOtherString";
            this.lblSomeOtherString.Size = new System.Drawing.Size(34, 13);
            this.lblSomeOtherString.TabIndex = 127;
            this.lblSomeOtherString.Text = "Other String:";
            //
            // lblomeOtherBoolean
            //
            this.lblSomeOtherBoolean.AutoSize = true;
            this.lblSomeOtherBoolean.Location = new System.Drawing.Point(170, 105);
            this.lblSomeOtherBoolean.Name = "lblomeOtherBoolean";
            this.lblSomeOtherBoolean.Size = new System.Drawing.Size(31, 13);
            this.lblSomeOtherBoolean.TabIndex = 127;
            this.lblSomeOtherBoolean.Text = "Other Boolean:";
            // 
            // txtSomeOtherInt
            // 
            this.txtSomeOtherInt.Location = new System.Drawing.Point(247, 60);
            this.txtSomeOtherInt.Name = "txtSomeOtherInt";
            this.txtSomeOtherInt.Size = new System.Drawing.Size(100, 20);
            this.txtSomeOtherInt.TabIndex = 128;
            // 
            // txtSomeOtherString
            // 
            this.txtSomeOtherString.Location = new System.Drawing.Point(247, 80);
            this.txtSomeOtherString.Name = "txtSomeOtherString";
            this.txtSomeOtherString.Size = new System.Drawing.Size(100, 20);
            this.txtSomeOtherString.TabIndex = 129;
            // 
            // chkSomeOtherBoolean
            // 
            this.chkSomeOtherBoolean.AutoSize = true;
            this.chkSomeOtherBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkSomeOtherBoolean.Location = new System.Drawing.Point(247, 105);
            this.chkSomeOtherBoolean.Name = "chkSomeOtherBoolean";
            this.chkSomeOtherBoolean.Size = new System.Drawing.Size(65, 17);
            this.chkSomeOtherBoolean.TabIndex = 130;
            this.chkSomeOtherBoolean.Text = "";
            this.chkSomeOtherBoolean.UseVisualStyleBackColor = true;
            // 
            // lblStillAnotherInt
            // 
            this.lblStillAnotherInt.AutoSize = true;
            this.lblStillAnotherInt.Location = new System.Drawing.Point(360, 65);
            this.lblStillAnotherInt.Name = "lblStillAnotherInt";
            this.lblStillAnotherInt.Size = new System.Drawing.Size(31, 13);
            this.lblStillAnotherInt.TabIndex = 131;
            this.lblStillAnotherInt.Text = "Another Int:";
            // 
            // lblStillAnotherString
            // 
            this.lblStillAnotherString.AutoSize = true;
            this.lblStillAnotherString.Location = new System.Drawing.Point(360, 85);
            this.lblStillAnotherString.Name = "lblStillAnotherString";
            this.lblStillAnotherString.Size = new System.Drawing.Size(34, 13);
            this.lblStillAnotherString.TabIndex = 132;
            this.lblStillAnotherString.Text = "Another String:";
            //
            // lblStillAnotherBoolean
            //
            this.lblStillAnotherBoolean.AutoSize = true;
            this.lblStillAnotherBoolean.Location = new System.Drawing.Point(360, 105);
            this.lblStillAnotherBoolean.Name = "lblStillAnotherBoolean";
            this.lblStillAnotherBoolean.Size = new System.Drawing.Size(31, 13);
            this.lblStillAnotherBoolean.TabIndex = 132;
            this.lblStillAnotherBoolean.Text = "Another Boolean:";
            // 
            // txtStillAnotherInt
            // 
            this.txtStillAnotherInt.Location = new System.Drawing.Point(450, 60);
            this.txtStillAnotherInt.Name = "txtStillAnotherInt";
            this.txtStillAnotherInt.Size = new System.Drawing.Size(100, 20);
            this.txtStillAnotherInt.TabIndex = 133;
            // 
            // txtStillAnotherString
            // 
            this.txtStillAnotherString.Location = new System.Drawing.Point(450, 80);
            this.txtStillAnotherString.Name = "txtStillAnotherString";
            this.txtStillAnotherString.Size = new System.Drawing.Size(100, 20);
            this.txtStillAnotherString.TabIndex = 134;
            // 
            // chkStillAnotherBoolean
            // 
            this.chkStillAnotherBoolean.AutoSize = true;
            this.chkStillAnotherBoolean.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkStillAnotherBoolean.Location = new System.Drawing.Point(450, 105);
            this.chkStillAnotherBoolean.Name = "chkStillAnotherBoolean";
            this.chkStillAnotherBoolean.Size = new System.Drawing.Size(65, 17);
            this.chkStillAnotherBoolean.TabIndex = 135;
            this.chkStillAnotherBoolean.Text = "";
            this.chkStillAnotherBoolean.UseVisualStyleBackColor = true;
            //
            // cmdFont
            //
            this.cmdFont.Location = new System.Drawing.Point(5, 125);
            this.cmdFont.Name = "cmdFont";
            this.cmdFont.Size = new System.Drawing.Size(75, 23);
            this.cmdFont.TabIndex = 136;
            this.cmdFont.Text = "Font";
            this.cmdFont.UseVisualStyleBackColor = true;
            this.cmdFont.Click += new System.EventHandler(this.cmdFont_Click);
            //
            // cmdColor
            //
            this.cmdColor.Location = new System.Drawing.Point(170, 125);
            this.cmdColor.Name = "cmdColor";
            this.cmdColor.Size = new System.Drawing.Size(75, 23);
            this.cmdColor.TabIndex = 136;
            this.cmdColor.Text = "Color";
            this.cmdColor.UseVisualStyleBackColor = true;
            this.cmdColor.Click += new System.EventHandler(this.cmdColor_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.chkStillAnotherBoolean);
            this.Controls.Add(this.txtStillAnotherString);
            this.Controls.Add(this.lblSomeBoolean);
            this.Controls.Add(this.txtStillAnotherInt);
            this.Controls.Add(this.lblStillAnotherString);
            this.Controls.Add(this.lblStillAnotherInt);
            this.Controls.Add(this.chkSomeOtherBoolean);
            this.Controls.Add(this.txtSomeOtherString);
            this.Controls.Add(this.txtSomeOtherInt);
            this.Controls.Add(this.lblSomeOtherBoolean);
            this.Controls.Add(this.lblSomeOtherString);
            this.Controls.Add(this.lblSomeOtherInt);
            this.Controls.Add(this.cmdRun);
            this.Controls.Add(this.chkSomeBoolean);
            this.Controls.Add(this.txtSomeString);
            this.Controls.Add(this.txtSomeInt);
            this.Controls.Add(this.lblStillAnotherBoolean);
            this.Controls.Add(this.lblSomeString);
            this.Controls.Add(this.lblSomeInt);
            this.Controls.Add(this.cmdFont);
            this.Controls.Add(this.cmdColor);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.toolbar);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MonoApp1";
            this.Load += new System.EventHandler(this.View_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.View_FormClosing);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.toolbar.ResumeLayout(false);
            this.toolbar.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileNew;
        private System.Windows.Forms.ToolStripMenuItem menuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem menuFileSave;
        private System.Windows.Forms.ToolStripMenuItem menuFileSaveAs;
        private System.Windows.Forms.ToolStripSeparator menuFileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuFilePrint;
        private System.Windows.Forms.ToolStripSeparator menuFileSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.ToolStripMenuItem menuEdit;
        private System.Windows.Forms.ToolStripMenuItem menuEditUndo;
        private System.Windows.Forms.ToolStripMenuItem menuEditRedo;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator0;
        private System.Windows.Forms.ToolStripMenuItem menuEditSelectAll;
        private System.Windows.Forms.ToolStripMenuItem menuEditCut;
        private System.Windows.Forms.ToolStripMenuItem menuEditCopy;
        private System.Windows.Forms.ToolStripMenuItem menuEditPaste;
        private System.Windows.Forms.ToolStripMenuItem menuEditDelete;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuEditFind;
        private System.Windows.Forms.ToolStripMenuItem menuEditReplace;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuEditRefresh;
        private System.Windows.Forms.ToolStripSeparator menuEditSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuEditProperties;
        private System.Windows.Forms.ToolStripMenuItem menuEditPreferences;
        private System.Windows.Forms.ToolStripMenuItem menuWindow;
        private System.Windows.Forms.ToolStripMenuItem menuWindowNewWindow;
        private System.Windows.Forms.ToolStripMenuItem menuWindowTile;
        private System.Windows.Forms.ToolStripMenuItem menuWindowCascade;
        private System.Windows.Forms.ToolStripMenuItem menuWindowArrangeAll;
        private System.Windows.Forms.ToolStripSeparator menuWindowSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuWindowHide;
        private System.Windows.Forms.ToolStripMenuItem menuWindowShow;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuHelpContents;
        private System.Windows.Forms.ToolStripMenuItem menuHelpIndex;
        private System.Windows.Forms.ToolStripMenuItem menuHelpOnlineHelp;
        private System.Windows.Forms.ToolStripSeparator menuHelpSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuHelpLicenceInformation;
        private System.Windows.Forms.ToolStripMenuItem menuHelpCheckForUpdates;
        private System.Windows.Forms.ToolStripSeparator menuHelpSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuHelpAbout;
        private System.Windows.Forms.ToolStrip toolbar;
        private System.Windows.Forms.ToolStripButton buttonFileNew;
        private System.Windows.Forms.ToolStripButton buttonFileOpen;
        private System.Windows.Forms.ToolStripButton buttonFileSave;
        private System.Windows.Forms.ToolStripButton buttonFilePrint;
        private System.Windows.Forms.ToolStripSeparator buttonSeparator0;
        private System.Windows.Forms.ToolStripButton buttonEditUndo;
        private System.Windows.Forms.ToolStripButton buttonEditRedo;
        private System.Windows.Forms.ToolStripButton buttonEditCut;
        private System.Windows.Forms.ToolStripButton buttonEditCopy;
        private System.Windows.Forms.ToolStripButton buttonEditPaste;
        private System.Windows.Forms.ToolStripButton buttonEditDelete;
        private System.Windows.Forms.ToolStripButton buttonEditFind;
        private System.Windows.Forms.ToolStripButton buttonEditReplace;
        private System.Windows.Forms.ToolStripButton buttonEditRefresh;
        private System.Windows.Forms.ToolStripButton buttonEditPreferences;
        private System.Windows.Forms.ToolStripButton buttonEditProperties;
        private System.Windows.Forms.ToolStripSeparator buttonSeparator1;
        private System.Windows.Forms.ToolStripButton buttonHelpContents;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarStatusMessage;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarErrorMessage;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarCustomMessage;
        private System.Windows.Forms.ToolStripProgressBar StatusBarProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarActionIcon;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarDirtyMessage;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarNetworkIndicator;
        private System.Windows.Forms.Label lblSomeInt;
        private System.Windows.Forms.Label lblSomeString;
        private System.Windows.Forms.Label lblSomeBoolean;
        private System.Windows.Forms.TextBox txtSomeInt;
        private System.Windows.Forms.TextBox txtSomeString;
        private System.Windows.Forms.CheckBox chkSomeBoolean;
        private System.Windows.Forms.Button cmdRun;
        private System.Windows.Forms.Label lblSomeOtherInt;
        private System.Windows.Forms.Label lblSomeOtherString;
        private System.Windows.Forms.Label lblSomeOtherBoolean;
        private System.Windows.Forms.TextBox txtSomeOtherInt;
        private System.Windows.Forms.TextBox txtSomeOtherString;
        private System.Windows.Forms.CheckBox chkSomeOtherBoolean;
        private System.Windows.Forms.Label lblStillAnotherInt;
        private System.Windows.Forms.Label lblStillAnotherString;
        private System.Windows.Forms.Label lblStillAnotherBoolean;
        private System.Windows.Forms.TextBox txtStillAnotherInt;
        private System.Windows.Forms.TextBox txtStillAnotherString;
        private System.Windows.Forms.CheckBox chkStillAnotherBoolean;
        private System.Windows.Forms.Button cmdFont;
        private System.Windows.Forms.Button cmdColor;

    }
}