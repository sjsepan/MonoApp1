﻿using System;
using System.Windows.Forms;

namespace MonoApp1
{
    class MainClass
    {
         [STAThread]
        public static int Main(string[] args)
        {
            //default to fail code
            int returnValue = -1;

            try
            {
                MainWindow mainWindow = new MainWindow();
                Application.Run(mainWindow);

                //return success code
                returnValue = 0;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
            return returnValue;
        }
    }
}
