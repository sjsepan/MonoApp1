# readme for MonoApp1 v0.1

## About

Desktop GUI app prototype, on Linux(/Win/Mac), in C# / Mono / WinForms, using VSCode

![MonoApp1.png](./MonoApp1.png?raw=true "Screenshot")

### Notes

I have used the partial-class code-only implementation because...

1) it is closest to the way that Visual Studio seemed to do things, and
2) intellisense on the classes should be available for code-only.

I will be coding MainWindow.cs to include a WinForms designer (MainWindow.designer.cs) as an example of what I would hope some form designer / editor would consume / produce.
I will also be including an associated .resx file for icons.

### VSCode

To nest files associated with forms, use the setting 'Features/Explorer/File Nesting: Patterns':
Item="*.cs", Value="${basename}.resx,${basename}.Designer.cs"

C# support: see
<https://open-vsx.org/extension/muhammad-sammy/csharp>

### Instructions for downloading/installing Mono

Locate / install 'mono-complete' from your distribution's package manager.

If you are using Visual Basic, then you will likely also want to install ​mono-vbnc​ for dependencies such as ​Microsoft.VisualBasic​.
sudo apt-get install mono-vbnc

### Build / Run from terminal

Build from the same folder as the .csproj, with "xbuild MonoApp1.csproj".
If you try to run ('mono MonoApp1.exe') from MonoApp1 project folder, you will get an error "Cannot open assembly 'MonoApp1.exe': No such file or directory.". Make sure you run from the bin/Debug sub-folder.

### Issues

~When laying out statusbar, menubar and toolbar, docking of the latter two at the top of the form will be governed by the order in which they are added to the form's Controls collection. Note that the most recently added will get first shot at the top of the form, so add the menu after the toolbar.

### History

0.1:
~initial release

Steve Sepan
ssepanus@yahoo.com
6/3/2022
